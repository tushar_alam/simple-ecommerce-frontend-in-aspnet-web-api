﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Maxteo_Ecommerce_Backend.Models;

namespace Maxteo_Ecommerce_Backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InquriesController : ApiController
    {
        private Max_EcommerceEntities db = new Max_EcommerceEntities();

        // GET: api/Inquries
        public IEnumerable<Inqury> GetInquries()
        {
            return db.Inquries.ToList();
        }

        // GET: api/Inquries/5
        [ResponseType(typeof(Inqury))]
        public IHttpActionResult GetInqury(int id)
        {
            Inqury inqury = db.Inquries.Find(id);
            if (inqury == null)
            {
                return NotFound();
            }

            return Ok(inqury);
        }
        [HttpGet]
        [Route("api/Inquries/ForRead/{id}")]
        [ResponseType(typeof(Inqury))]
        public IHttpActionResult GetInquryForRead(int id)
        {
            Inqury inqury = db.Inquries.Find(id);

            if (inqury == null)
            {
                return NotFound();
            }
            inqury.Status = 2;

            db.Entry(inqury).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(inqury);
        }

        [HttpPost]
        [Route("api/Inquries/EditInqury")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(Inqury inquiry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(inquiry).State = EntityState.Modified;
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);

        }

        // POST: api/Products
        [HttpPost]
        [Route("api/Inquries/PostInqury")]
        [ResponseType(typeof(Inqury))]
        public IHttpActionResult PostProduct(Inqury inquiry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Inquries.Add(inquiry);
            db.SaveChanges();

            return Ok(inquiry);
        }


        // DELETE: api/Inquries/5
        [ResponseType(typeof(Inqury))]
        public IHttpActionResult DeleteInqury(int id)
        {
            Inqury inqury = db.Inquries.Find(id);
            if (inqury == null)
            {
                return NotFound();
            }

            db.Inquries.Remove(inqury);
            db.SaveChanges();

            return Ok(inqury);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InquryExists(int id)
        {
            return db.Inquries.Count(e => e.Id == id) > 0;
        }
    }
}